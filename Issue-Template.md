(Copy and paste the source of this file into an issue)

# Title: short but descriptive

## Overview

A brief statement of what is to be done and why it is valuable to whom.
If you are struggling to start, try using the "story form":

```
As a ROLE
I need WHAT
so that WHY
```

## Plan

A to do list of steps that must be completed for this to be considered
done. Include products produced and where on GitLab they will live.
Optionally add team members to items.

1. [x] One
2. [ ] Two


## Other

- Assign a weight.
- Optionally assign team members.
- Link to related issues (parents, blockers, children).
- Assign labels to categorize the issue to help others understand its context.
- Update the description, title, to-do, labels, etc, as necessary to keep
    the issue up to date with the current understanding. (Don't change the weight.)
- Comment on issue with progress, failures, suggestions, resources, etc.
