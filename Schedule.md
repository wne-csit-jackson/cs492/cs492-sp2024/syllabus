## Rough, Tentative Schedule

Below is a rough schedule of topics. A more detailed, up-to-date
schedule will be kept along with our class notes. Please follow along there.


* Week 1 - Product Increment Planning
* Week 2 - Sprint 1 Planning
* Week 3 - Sprint 1
* Week 4 - Sprint 1
* Week 5 - Sprint 1 Review & Retro
* Week 6 - Sprint 2 Planning
* Week 7 - Sprint 2
* Week 8 - Sprint 2
* Week 9 - HOLIDAY
* Week 10 - Sprint 2 Review & Retro
* Week 11 - Sprint 3 Planning
* Week 12 - Sprint 3
* Week 13 - Sprint 3
* Week 14 - Sprint 3 Review & Retro
* Week 15 - Presentations
* Week 16 - Paper due
