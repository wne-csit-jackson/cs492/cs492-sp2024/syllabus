# Syllabus - CS 492

Spring 2024

include::Instructor.md[]

include::Course.md[]

include::Materials.md[]

include::Grading.md[]

include::Sprint-Evaluation.asciidoc[]

include::Appeals.md[]

include::Accessibility.md[]

include::Privacy.md[]

include::Misconduct.md[]

include::Changes.md[]

include::Extenuating-Circumstances.md[]

include::Schedule.md[]
