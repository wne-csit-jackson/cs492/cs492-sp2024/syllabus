## Instructor Information

- Stoney Jackson, PhD
- Email: [Stoney.Jackson@wne.edu](mailto:Stoney.Jackson@wne.edu)
- Appointments: [https://stoneyjackson.youcanbook.me/](https://stoneyjackson.youcanbook.me/)
- Office: H207b and class Discord server

### Office Hours

Monday through Thursday, 11:00a-12:30p ET

During office hours, I am available in our class Discord server and
in my office. You do not need an appointment to meet with me during
these hours. In-person takes priority.

### Outside Office Hours

You may also contact me outside office hours on Discord, or by email.
Typically I check email and Discord in the morning when I first get to
the office. Please allow up to 1 business day for a response.
