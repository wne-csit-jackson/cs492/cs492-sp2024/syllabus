## Required Materials

### Free

Most of the materials for this course are free.

- Discord
- Git for Windows
- GitLab account
- GitHub account
- GitPod account

### Non-Free

- A personal computer with a supported version of Windows, MacOS, or Linux
  (but not Chromebook). If you do not have a personal computer that will
  run the software required by this course, you will need to plan to use
  one of the computer labs (H103 or H201) while they are not being used
  for a class.
