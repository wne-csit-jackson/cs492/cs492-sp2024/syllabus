## Course Information

CS 492 - Computer Science Capstone

This class will be run in a fashion similar to a production-grade software project. We will make some minor adjustments due to the fact that developers are students and this class makes up only part of their full-time effort. The goal is for students to gain experience developing a real-world project. For spring 2023, we will front-end load the course with learning about open source and GitLab in order to prepare students to enter the development environment. We will have three sprints, each approximately 4.5 weeks in length.

3 cr.

