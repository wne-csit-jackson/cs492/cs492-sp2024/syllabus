## Assessment

### Grading
- 20% Sprint 1
- 25% Sprint 2
- 25% Sprint 3
- 10% Presentation
- 20% Final Essay

### Grading Scale

The following scale is used to map overall scores to letter grades. The top is
the minimum overall percentage needed to earn the letter grade below. Overall
scores are rounded to the nearest percent before converting to a letter grade.

[%header,format=csv]
|===
0, 60, 67, 70, 73, 77, 80, 83, 87, 90, 93
F, D, D+, C-, C, C+, B-, B, B+, A-, A
|===

### Presentation

You and your team will present the state of your team's efforts over the semester.

### Final Essay

There is a final essay related to your experience in this course.
Details of this will be given near the midterm.
