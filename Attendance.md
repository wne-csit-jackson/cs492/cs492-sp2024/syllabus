## Attendance

Attendance and participation is required both in class, and during team activities outside of class. Your attendance and participation will be assessed as part of the evaluation of each sprint.
